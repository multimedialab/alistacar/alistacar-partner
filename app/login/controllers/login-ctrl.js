'use strict';
angular.module('login').controller('LoginCtrl', function($scope, $state, $usuario, $message, $push, $ionicHistory) {

    $scope.sesion = true;

    $scope.ingresar = function(usuario, contrasena) {
        if (!usuario || !contrasena) {
            return $message.alert("Por favor complete todos los campos para ingresar.");
        }

        $usuario.login(usuario, contrasena).then(function() {
            $push.registro(function() {
                $ionicHistory.clearCache().then(function() {
                    $state.go('servicios.solicitudes', {}, { reload: true });
                });
            });
        }).catch(function() {
            return $message.alert("El usuario o la contraseña son invalidos, veririquelos e intentelo nuevamente.");
        });
    }
});