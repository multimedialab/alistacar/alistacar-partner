'use strict';
angular.module('servicios', [
  'ionic',
  'ngCordova',
  'ui.router',
  // TODO: load other modules selected during generation
])
.config(function ($stateProvider) {

  // ROUTING with ui.router
  $stateProvider
    // this state is placed in the <ion-nav-view> in the index.html
    .state('servicios', {
      url: '/servicios',
      abstract: true,
      templateUrl: 'servicios/templates/tabs.html',
      controller: 'TabsCtrl'
    })
    .state('servicios.solicitudes', {
      url: '/solicitudes',
      views: {
        'servicios-solicitudes': {
          templateUrl: 'servicios/templates/solicitudes.html',
          controller: 'SolicitudesCtrl as scope'
        }
      }
    })
    .state('servicios.agenda', {
      url: '/agenda',
      views: {
        'servicios-agenda': {
          templateUrl: 'servicios/templates/agenda.html',
          controller: 'AgendaCtrl as scope'
        }
      }
    })
    .state('servicios.historico', {
      url: '/historico',
      views: {
        'servicios-historico': {
          templateUrl: 'servicios/templates/historico.html',
          controller: 'HistoricoCtrl as scope'
        }
      }
    })
});
