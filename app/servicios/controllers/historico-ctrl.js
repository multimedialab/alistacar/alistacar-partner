'use strict';
angular.module('servicios').filter('reverse', function() {
  return function(items) {
    return items.slice().reverse();
  };
}).controller('HistoricoCtrl', function ($scope, $server, $localStorage) {
  
  $scope.todos = $server.data;
  $scope.empresa = $localStorage.punto.empresa;
  
});
