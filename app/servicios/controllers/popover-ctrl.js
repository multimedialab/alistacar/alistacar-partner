'use strict';
angular.module('servicios').controller('PopoverCtrl', function ($scope, $usuario) {

  $scope.usuario = $usuario;
  if ($scope.$root && $scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
    $scope.$apply();
  }

});
