'use strict';
angular.module('servicios').controller('AgendaCtrl', function($server, $message, $localStorage, $ionicLoading) {

    var scope = this;

    scope.todos = $server.data;
    scope.empresa = $localStorage.punto.empresa;

    scope.isCancelable = function(fecha) {
        var fecha_hoy = new Date();
        var fecha_cancelable = new Date(fecha);
        fecha_cancelable.setFullYear(fecha_hoy.getFullYear());
        fecha_cancelable.setMinutes(fecha_cancelable.getMinutes() - 15);
        var end_cancelable = new Date(fecha_cancelable);
        end_cancelable.setMinutes(end_cancelable.getMinutes() + 30);

        return (
            fecha_cancelable.getTime() < fecha_hoy.getTime() &&
            fecha_hoy.getTime() < end_cancelable.getTime()
        ) ? false : true;
    }

    scope.cancelar = function(id) {
        $ionicLoading.show();
        console.log("Terminar:", id);
        $server.rechazarServicio({
            id: id
        }, function() {
            $ionicLoading.hide();
            $message.alert("Respuesta enviada");
        });
    }

    scope.callNumber = function(tel) {
        window.plugins.CallNumber.callNumber(function(res) {}, function(err) {}, tel.toString(), false);
    }

    scope.terminar = function(servicio, index) {
        $message.prompt("Inserte código de usuario", "").then(function(codigo) {
            if (codigo != servicio.telefono_cliente.slice(-4))
                return $message.alert("El código ingresado es incorrecto");

            $server.terminar(servicio.id_servicio, function(res) {
                scope.todos._agenda.splice(index, 1);
            });
        });
    }

});