'use strict';
angular.module('servicios').controller('TabsCtrl', function ($scope, $ionicPopover, $localStorage, $push, $state, $ionicHistory) {

  $ionicPopover.fromTemplateUrl('servicios/templates/popover.html', {
    scope: $scope,
  }).then(function(popover) {
    $scope.popover = popover;
  });

  $scope.logout = function () {
    delete $localStorage.punto;
    $push.unregistro();
    $scope.popover.hide();
    $ionicHistory.clearCache().then(function(){
      $state.go('login', {}, {reload:true});
    });
  }

});
