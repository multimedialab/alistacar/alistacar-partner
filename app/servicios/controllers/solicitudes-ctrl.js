'use strict';
angular.module('servicios').controller('SolicitudesCtrl', function ($server, $message, $localStorage) {

  var scope = this;

  scope.todos = $server.data;
  scope.empresa = $localStorage.punto.empresa;

  scope.aceptarServicio = function (index, id) {
    $server.aceptarServicio({ id: id }, function () {
      $message.alert("Respuesta enviada");
    });
  }

  scope.rechazarServicio = function (index, id) {
    $server.rechazarServicio({ id: id }, function () {
      $message.alert("Respuesta enviada");
    });
  }

  scope.callNumber = function (tel) {
    window.plugins.CallNumber.callNumber(function (res) { }, function (err) { }, tel.toString(), false);
  }

  setTimeout(function () {
    if (Object.keys($server.data).length === 0)
      $server.cargarServicios();
  }, 500);

});
