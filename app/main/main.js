'use strict';
angular.module('main', [
    'ionic',
    'ngCordova',
    'ui.router'
]).run(function($state, $localStorage, $usuario, $push, $ionicPlatform, $status, $cordovaNetwork, $ionicPopup, $rootScope) {

    /*El  inicio de la aplicacion se realiza con addEventListener('deviceready') 
    la funcion $ionicPlatform.ready() empaqueta este metodo y colocarla otra vez lo duplica*/
    if ($usuario.getAuth()) {
        $state.go('servicios.solicitudes');
    } else {
        $state.go('login');
    }

    document.addEventListener('deviceready', function() {
        if (ionic.Platform.platform() == "ios")
            navigator.splashscreen.hide();
        else
            setTimeout(function() { navigator.splashscreen.hide(); }, 1000);
    }, false);


});