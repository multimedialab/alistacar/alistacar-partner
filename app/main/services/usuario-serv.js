'use strict';
angular.module('main').service('$usuario', function ($q, $localStorage) {

  var usuario = this;
  
  usuario.id_punto_de_servicio = ($localStorage.punto && $localStorage.punto.id_punto_de_servicio) ? $localStorage.punto.id_punto_de_servicio : null;
  usuario.nombre = ($localStorage.punto && $localStorage.punto.nombre) ? $localStorage.punto.nombre : null;
  usuario.imagen = ($localStorage.punto && $localStorage.punto.imagen) ? $localStorage.punto.imagen : null;
  
  usuario.login = function (email, pase) {
    return $q(function (resolve, reject) {
      io.socket.post('/partner/login-partner', {usuario: email, password: pase}, function (response, data) {
        if(data.statusCode != 200 || response == undefined){
          reject();
        } else {
          usuario.id_punto_de_servicio = response.id_punto_de_servicio;
          usuario.nombre = response.nombre;
          usuario.imagen = response.imagen;
          $localStorage.punto = {id_punto_de_servicio: response.id_punto_de_servicio, nombre: response.nombre, imagen: response.imagen, empresa: response.es_master};
          resolve();
        }
      });
    });
  }
  
  usuario.getAuth = function () {
    return $localStorage.punto;
  }
  
  usuario.registrarToken = function (token, plataforma) {
    io.socket.post('/partner/registrar-token/', {id: usuario.id_punto_de_servicio, token: token, plataforma: plataforma});
  }
  
  usuario.borrarToken = function (token) {
    io.socket.post('/partner/borrar-token/', {id: usuario.id_punto_de_servicio, token: token});
  }

});
