'use strict';
angular.module('main').service('$status', function($rootScope, $ionicLoading) {

    var detection = {};
    var activo = true;
    var networkConnected = true;

    var serv = this;
    document.addEventListener("deviceready", function() {

        document.addEventListener("resume", function() {
            if (activo) return;

            activo = true;
            $rootScope.$broadcast('appforeground');
        }, false);

        document.addEventListener("pause", function() {
            console.log('onPause');
            if (!activo) return;

            activo = false;
            $rootScope.$broadcast('appbackground');
        }, false);

        window.addEventListener('online', function() {
            console.log('onNetworkOnline');
            if (networkConnected) return;
            networkConnected = true;
            $ionicLoading.hide();
            $rootScope.$broadcast('networkonline');
        });

        window.addEventListener('offline', function() {
            if (!networkConnected) return;

            networkConnected = false;
            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner> <br/> Necesitas internet para usar Alistacar<br/> Por favor revisa tu conexión'
            })
            $rootScope.$broadcast('networkoffline');
        });

    }, false);

});