'use strict';
angular.module('main').factory('$push', function($usuario, $rootScope) {
    var service = {};
    var push = null;
    var inited = false;

    document.addEventListener('deviceReady', function() {
        if (!window.cordova)
            return;


        try {
            push = PushNotification.init({
                android: {
                    senderID: "403189495910",
                    forceShow: true,
                    clearBadge: true
                },
                ios: {
                    alert: true,
                    badge: true,
                    sound: true,
                    clearBadge: true
                }
            });

            push.on('registration', function(data) {
                inited = true;
                push.token = data.registrationId;
                PushNotification.hasPermission(function() {
                    console.log('token', push.token);
                    if ($usuario.getAuth())
                        $usuario.registrarToken(push.token, ionic.Platform.platform());
                });
            });

            push.on('notification', function(data) {
                console.info(data);
                setTimeout(function() { $rootScope.$apply(); }, 1000);
            });

            push.getApplicationIconBadgeNumber(function(n) {
                service.badge = 0;
            }, function() {
                service.badge = 0;
            });

        } catch (e) {
            console.log('Error initializing $push:', e);
        }
    });

    service.registro = function(callback) {
        if (!window.cordova) return callback();
        if (!inited) return callback();

        PushNotification.hasPermission(function() {
            console.log('token', push.token);
            $usuario.registrarToken(push.token, ionic.Platform.platform());
            return callback();
        });
    }

    service.unregistro = function() {
        if (!push)
            return;
        $usuario.borrarToken(push.token);
        push.unregister();
    }

    return service;

});