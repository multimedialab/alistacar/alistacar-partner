'use strict';
angular.module('main').factory('$server', function($usuario, $rootScope, $message, $ionicLoading, $state, $timeout) {
    var server = this;
    console.log("esto se llama una vez");
    server.data = {};

    $rootScope.$on('appforeground', function(event, args) {
        console.log('onAppForeground');
        io.socket._raw.disconnect();
        io.socket._raw.connect();
        
        server.cargarServicios();
    });
    
    io.socket.on('connect', function() {
        console.log('-->>>>>>> onConnect');
        $message.alert("Ahora estás conectado", "long", "bottom");
        server.cargarServicios();
    });

    io.socket.on('disconnect', function() {
        console.log('-->>>>>>> onDisconnect',io.socket);
        $message.alert("Reconectando", "long", "bottom");
    });

    io.socket.on("message", function(servicio) {
        if (servicio.cancelado == true) {
            server.data._servicios = _.without(server.data._servicios, _.findWhere(server.data._servicios, { id_servicio: servicio.id_servicio }));
            server.data._agenda = _.without(server.data._agenda, _.findWhere(server.data._agenda, { id_servicio: servicio.id_servicio }));
        } else if (servicio.estado == false) {
            server.data._servicios.push(servicio);
        } else if (servicio.estado == true && servicio.terminado == false) {
            server.data._servicios = _.without(server.data._servicios, _.findWhere(server.data._servicios, { id_servicio: servicio.id_servicio }));
            server.data._agenda.push(servicio);
        } else if (servicio.estado == true && servicio.terminado == true) {
            server.data._agenda = _.without(server.data._agenda, _.findWhere(server.data._agenda, { id_servicio: servicio.id_servicio }));
            server.data._historico.push(servicio);
        }
        $rootScope.$apply();
        setTimeout(function() { $rootScope.$apply(); }, 1000);
    });

    server.cargarServicios = function() {
        $ionicLoading.show();
        io.socket.get('/partner/servicios/' + $usuario.id_punto_de_servicio, function(response, data) {
            console.info("Cargo servicios correctamente");
            server.data._servicios = _.where(response, { status: 'Solicitado' });
            server.data._agenda = _.where(response, { status: 'Aceptado' });
            server.data._historico = _.where(response, { status: 'Terminado' });
            $rootScope.$apply();
            $ionicLoading.hide();
            setTimeout(function() { $rootScope.$apply(); }, 1000);
        });
    }

    // --------------------------- Servicios para aceptar o rechazar una solicitud --------------------------------------

    server.aceptarServicio = function(datos, callback) {
        $ionicLoading.show({
            content: 'Loading...',
            animation: 'fade-out',
            showBackdrop: false,
            maxWidth: 200,
            showDelay: 500,
            duration: 3000
          });
        if (io.socket.isConnected()) {
            $message.alert("Respuesta enviada", "long", "bottom");  
            io.socket.post('/partner/aceptarservicio', datos, function(response, data) {
                console.log('asdasd:', response, data)
                callback(response);
                $ionicLoading.hide();
            });
        } else {
            $ionicLoading.hide();
            callback(null);
        }
    };

    server.rechazarServicio = function(datos, callback) {
        $ionicLoading.show({
            content: 'Loading...',
            animation: 'fade-out',
            showBackdrop: false,
            maxWidth: 200,
            showDelay: 500,
            duration: 3000
          });
        if (io.socket.isConnected()) {
            $message.alert("Respuesta enviada", "long", "bottom");  
            io.socket.post('/partner/rechazarservicio', datos, function(response, data) {
                console.log('asdasd:', response, data)
                callback(response);
                $ionicLoading.hide();
            });
        } else {
            $ionicLoading.hide();
            callback(null);
        }
    };

    // ------------------------------------------------------------------------------------------------------------------

    server.terminar = function(id, callback) {
        $ionicLoading.show();
        if (io.socket.isConnected()) {
            io.socket.post('/partner/terminar', { id: id }, function(response) {
                callback(response);
                $ionicLoading.hide();
            });
        } else {
            callback(null);
            $ionicLoading.hide();
        }
    };

    server.registrarToken = function(token, plataforma) {
        io.socket.post('/partner/registrar-token/', { id: $usuario.id_punto_de_servicio, token: token, plataforma: plataforma }, function(response, data) {});
    }

    return server;

});