'use strict';

describe('module: main, service: Message', function () {

  // load the service's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate service
  var Message;
  beforeEach(inject(function (_Message_) {
    Message = _Message_;
  }));

  it('should do something', function () {
    expect(!!Message).toBe(true);
  });

});
