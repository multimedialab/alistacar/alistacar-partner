'use strict';

describe('module: main, service: Network', function () {

  // load the service's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate service
  var Network;
  beforeEach(inject(function (_Network_) {
    Network = _Network_;
  }));

  it('should do something', function () {
    expect(!!Network).toBe(true);
  });

});
