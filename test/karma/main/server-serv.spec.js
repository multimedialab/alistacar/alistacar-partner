'use strict';

describe('module: main, service: Server', function () {

  // load the service's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate service
  var Server;
  beforeEach(inject(function (_Server_) {
    Server = _Server_;
  }));

  it('should do something', function () {
    expect(!!Server).toBe(true);
  });

});
