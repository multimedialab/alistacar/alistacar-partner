'use strict';

describe('module: main, service: Notifications', function () {

  // load the service's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate service
  var Notifications;
  beforeEach(inject(function (_Notifications_) {
    Notifications = _Notifications_;
  }));

  it('should do something', function () {
    expect(!!Notifications).toBe(true);
  });

});
