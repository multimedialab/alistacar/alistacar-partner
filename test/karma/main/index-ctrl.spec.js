'use strict';

describe('module: main, controller: IndexCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var IndexCtrl;
  beforeEach(inject(function ($controller) {
    IndexCtrl = $controller('IndexCtrl');
  }));

  it('should do something', function () {
    expect(!!IndexCtrl).toBe(true);
  });

});
