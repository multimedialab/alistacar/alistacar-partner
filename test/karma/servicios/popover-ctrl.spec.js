'use strict';

describe('module: servicios, controller: PopoverCtrl', function () {

  // load the controller's module
  beforeEach(module('servicios'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var PopoverCtrl;
  beforeEach(inject(function ($controller) {
    PopoverCtrl = $controller('PopoverCtrl');
  }));

  it('should do something', function () {
    expect(!!PopoverCtrl).toBe(true);
  });

});
