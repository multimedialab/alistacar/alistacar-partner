'use strict';

describe('module: servicios, controller: ServiciosDebugCtrl', function () {

  // load the controller's module
  beforeEach(module('servicios'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var ServiciosDebugCtrl;
  beforeEach(inject(function ($controller) {
    ServiciosDebugCtrl = $controller('ServiciosDebugCtrl');
  }));

  describe('.grade()', function () {

    it('should classify asd as weak', function () {
      ServiciosDebugCtrl.password.input = 'asd';
      ServiciosDebugCtrl.grade();
      expect(ServiciosDebugCtrl.password.strength).toEqual('weak');
    });

    it('should classify asdf as medium', function () {
      ServiciosDebugCtrl.password.input = 'asdf';
      ServiciosDebugCtrl.grade();
      expect(ServiciosDebugCtrl.password.strength).toEqual('medium');
    });

    it('should classify asdfasdfasdf as strong', function () {
      ServiciosDebugCtrl.password.input = 'asdfasdfasdf';
      ServiciosDebugCtrl.grade();
      expect(ServiciosDebugCtrl.password.strength).toEqual('strong');
    });
  });

});
