'use strict';

describe('module: servicios, service: Servicios', function () {

  // load the service's module
  beforeEach(module('servicios'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate service
  var Servicios;
  var $timeout;
  beforeEach(inject(function (_Servicios_, _$timeout_) {
    Servicios = _Servicios_;
    $timeout = _$timeout_;
  }));

  describe('.changeBriefly()', function () {
    beforeEach(function () {
      Servicios.changeBriefly();
    });
    it('should briefly change', function () {
      expect(Servicios.someData.binding).toEqual('Yeah this was changed');
      $timeout.flush();
      expect(Servicios.someData.binding).toEqual('Yes! Got that databinding working');
    });
  });

});
