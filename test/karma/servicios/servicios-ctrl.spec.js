'use strict';

describe('module: servicios, controller: ServiciosCtrl', function () {

  // load the controller's module
  beforeEach(module('servicios'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var ServiciosCtrl;
  beforeEach(inject(function ($controller) {
    ServiciosCtrl = $controller('ServiciosCtrl');
  }));

  it('should do something', function () {
    expect(!!ServiciosCtrl).toBe(true);
  });

});
