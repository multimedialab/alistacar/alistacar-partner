'use strict';

describe('module: servicios, controller: SolicitudesCtrl', function () {

  // load the controller's module
  beforeEach(module('servicios'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var SolicitudesCtrl;
  beforeEach(inject(function ($controller) {
    SolicitudesCtrl = $controller('SolicitudesCtrl');
  }));

  it('should do something', function () {
    expect(!!SolicitudesCtrl).toBe(true);
  });

});
