'use strict';

describe('module: servicios, controller: HistoricoCtrl', function () {

  // load the controller's module
  beforeEach(module('servicios'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var HistoricoCtrl;
  beforeEach(inject(function ($controller) {
    HistoricoCtrl = $controller('HistoricoCtrl');
  }));

  it('should do something', function () {
    expect(!!HistoricoCtrl).toBe(true);
  });

});
